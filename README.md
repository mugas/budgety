<h1>Budge App</h1>

Another tutorial from [The Complete Javascript course 2018](https://www.udemy.com/the-complete-javascript-course/).

First tutorial where I used recent skills in the time of <em>Closure, IIFE, modules</em>.

It was a tutorial with a lot to take in that I want to use for my [food app](https://github.com/mugas/good_stuff_foods) and create my personalized budget app.


<p><strong>Skills used and/or reeinforced:</strong></p>

<li>Event handler</li>
<li>Data input</li>
<li>IIFE</li>
<li>Closures</li>
<li>Modules</li>
<li>DOM Manipulation</li>
<li>Keyword <em>this</em></li>
<li>Templates</li>
